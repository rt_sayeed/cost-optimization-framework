#!/bin/bash
####################################################################################
#
# AUTHOR: Shaikh Sayeed home.sayeed.pc@gmail.com
# DATE: 26-06-2024
# VERSION: 1.0.0
# DESCRIPTION: Validate if user has sudo privileges
#
####################################################################################
# CHANGELOG:
#  - Adding base directory location all files generated out of the script
#
####################################################################################
#
# Bootstrap scripts executes the initialization phase of application/program.
#

# Function declaration
helper_func(){
  echo ""
  echo "USAGE:"
  echo "  sudo ./$0 --action <start|stop>"    
}


# Variable definition
MAIL_RECIPIENT="home.sayeed.pc@gmail.com,mdsayeed07@hotmail.com"
MAIL_FROM="noreply-cof@rtlab.com"
MAIL_CONTENT="./rendered-email-content.html"

# Check if script run with sudo
. libs/sudo-validate.sh


# Main program
if [[ "${#}" -eq 0 ]]; then
  echo "ERROR: Missing required script parameter."
  helper_func
  exit 1
fi

if [[ ${1} == "--action" ]]; then
  SCRIPT_ACTION=${2}
else
  echo "ERROR: Unrecognized parameter supplied to the script."
  helper_func
  exit 1
fi

# Install dependencies
. libs/install_dependencies.sh

# # Install cloud cli tools
. libs/install-aws-cli.sh
. libs/install-az-cli.sh

# # Authenticate to cloud
. libs/csp-authentication.sh

# Authenticate to cloud
if [[ ${SCRIPT_ACTION} == "stop" ]]; then
  . libs/shutdown-instances.sh

elif [[ ${SCRIPT_ACTION} == "start" ]]; then
. libs/start-instances.sh

else
  echo "ERROR: Wrong value provided paramter --action: ${SCRIPT_ACTION}."
  helper_func
  exit 1
fi

# Capture instance state and create csv report
. libs/capture-instance-state-csv.sh

# Generate HTML status report for email notification
. libs/generate-html-report.sh

# Send email notification
if [[ ! -f ${MAIL_CONTENT} ]]; then
  echo "ERROR: Email template file '${MAIL_CONTENT}' missing/not found."
  exit 1
fi

sendmail $MAIL_RECIPIENT <<EOF
From: $MAIL_FROM
To: $MAIL_RECIPIENT
Subject: Daily Instance Schedule Report
Content-Type: text/html

$(cat ${MAIL_CONTENT})
EOF