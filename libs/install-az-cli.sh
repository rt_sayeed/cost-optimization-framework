#!/bin/bash
#
#

# Define variables
SCRIPT_DIR="/opt/scripts"

echo "INFO: Installing azure command line utilities."

# Check if az cli is installed
command -v az > /dev/null

RETURN_CODE=${?}


if [[ ${RETURN_CODE} -ne 0 ]]; then
  echo "INFO: Installing az cli..."

  # Download installation script
  [[ -d $SCRIPT_DIR ]] || mkdir $SCRIPT_DIR

  curl -sL https://aka.ms/InstallAzureCLIDeb > $SCRIPT_DIR/install_az.sh
  chmod +x $SCRIPT_DIR/install_az.sh

  $SCRIPT_DIR/install_az.sh > /dev/null

  # Post installation-check
  if command -v az > /dev/null; then
    echo "INFO: Az cli successfully installed."
  else
    echo "ERROR: Az cli installation failed."
    exit 1
  fi

else
  echo "INFO: Az cli is already installed, skipping installation."
fi