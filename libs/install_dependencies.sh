#!/bin/bash
#
#
#

# ------------------ FUNCTION --------------------------------------------
# NAME:
# DESCRIPTION:
# ------------------------------------------------------------------------
install_yq(){
  if ! command -v yq > /dev/null; then
    wget -q https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64
    chmod +x yq_linux_amd64
    mv yq_linux_amd64 /usr/local/bin/yq
  fi
}

# Define variables
YQ_VERSION="4.40.3"

install_yq

sudo apt install -y jq pre-commit unzip > /dev/null