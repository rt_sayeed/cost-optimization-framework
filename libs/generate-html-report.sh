#!/bin/bash
#
#
#

CSV_INSTANCE_REPORT="instance-report.csv"
HTML_TEMPLATE_FILE="templates/email-content.html.tpl"
RENDERED_HTML_FILE="rendered-email-content.html"
TABLE_ROWS=""

for line in $(grep -vE "^CLOUD_PROVIDER" ${CSV_INSTANCE_REPORT}); do
  
  cloud_provider=$(echo ${line} | awk -F"," '{print $1}')
  instance_name=$(echo ${line} | awk -F"," '{print $2}')
  instance_ip=$(echo ${line} | awk -F"," '{print $3}')
  instance_state=$(echo ${line} | awk -F"," '{print $4}')
  instance_shutdown_time=$(echo ${line} | awk -F"," '{print $5}')
  instance_tag=$(echo ${line} | awk -F"," '{print $NF}')

  TABLE_ROWS+="
  <tr>
    <td>$cloud_provider</td>
    <td>$instance_name</td>
    <td>$instance_ip</td>
    <td>$instance_state</td>
    <td>$instance_shutdown_time</td>
    <td>$instance_tag</td>
  </tr>
  "
done

# Read HTML template
HTML_TEMPLATE=$(cat "${HTML_TEMPLATE_FILE}")

# Replace the placeholder with actual table rows using bash parameter expansion
# ${variable//pattern/replacement}
HTML_TEMPLATE=${HTML_TEMPLATE//__TABLE_ROWS_PLACEHOLDER__/$TABLE_ROWS}
HTML_TEMPLATE=${HTML_TEMPLATE//__TIMESTAMP__/$(date)}

# Write HTML template to the output file
echo "$HTML_TEMPLATE" > $RENDERED_HTML_FILE

echo "INFO: HTML email template with table rows and data has been generated. Output file: ${RENDERED_HTML_FILE}"
