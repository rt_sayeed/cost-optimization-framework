#!/bin/bash
#

# Define variables
CREDENTIAL_FILE=".cof/csp_auth.yaml"

#.cof/csp_auth.yaml
# ~/.cof/csp_auth.yaml


# Sanity checks
if [[ ! -f "$HOME/$CREDENTIAL_FILE" ]]; then
  echo "WARN: Authentication file not found $HOME/$CREDENTIAL_FILE."

  if [[ ! -f ${CREDENTIAL_FILE} ]]; then
    echo "ERROR: Authentication file not found $CREDENTIAL_FILE, aborting."
    exit 1
  else 
    echo "INFO: Authentication file found at $CREDENTIAL_FILE, Proceeding ..." 
  fi
fi


if ! command -v yq > /dev/null; then
  echo "ERROR: yq command not found, please install"
  exit 1
fi

# ----------------------------------
# Set AWS authentication variable
# ----------------------------------

echo "INFO: Trying to login on aws"

AWS_ACCESS_KEY_ID=$(yq -r '.auth_configs.aws.access_key' $CREDENTIAL_FILE)
AWS_SECRET_ACCESS_KEY=$(yq -r '.auth_configs.aws.secret_access_key' $CREDENTIAL_FILE)
AWS_REGION=$(yq -r '.auth_configs.aws.region' $CREDENTIAL_FILE)
AWS_OUTPUT=$(yq -r '.auth_configs.aws.output' $CREDENTIAL_FILE)

aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
aws configure set region $AWS_REGION
aws configure set output $AWS_OUTPUT


# Validate authentication
if aws sts get-caller-identity >/dev/null 2>&1; then
  IAM_USER_NAME=$(aws sts get-caller-identity --query 'Arn' --output text | awk -F"/" '{print $NF}')
  echo "INFO: Successfully logged in with IAM User ${IAM_USER_NAME}"
else
  echo "ERROR: Unexpected error occured while authenticating with IAM User"
  exit 1
fi


# ----------------------------------
# Set Azure authentication variable
# ----------------------------------

echo "INFO: Trying to login on azure"

AZURE_CLIENT_ID=$(yq -r ".auth_configs.azure.client_id" $CREDENTIAL_FILE)
AZURE_CLIENT_SECRET=$(yq -r ".auth_configs.azure.client_secret" $CREDENTIAL_FILE)
AZURE_SUBSCRIPTION_ID=$(yq -r ".auth_configs.azure.subscription_id" $CREDENTIAL_FILE)
AZURE_TENANT_ID=$(yq -r ".auth_configs.azure.tenant_id" $CREDENTIAL_FILE)

# Azure authentication
az login --service-principal \
  -u $AZURE_CLIENT_ID \
  -p $AZURE_CLIENT_SECRET \
  --tenant $AZURE_TENANT_ID \
  --allow-no-subscriptions > /dev/null

# Set active subscription 
az account set --subscription "${AZURE_SUBSCRIPTION_ID}"

if az account show >/dev/null 2>&1; then

  AZURE_APP_NAME=$(az ad sp show --id "$(az account show --query 'user.name' -o tsv)" --query appDisplayName -o tsv)
  echo "INFO: Successfully logged in with Azure App ${AZURE_APP_NAME}"
else
  echo "ERROR: Unexpected error occured while authenticating with AZ CLI command."
  exit 1
fi
