<!DOCTYPE html>
<html>
<head>
  <style>
    body { font-family: Calibri, sans-serif; }
    table {
      border-collapse: collapse;
      width: 80%;
    }
    th, td {
      padding: 8px;
      text-align: left;
      border-bottom: 1px solid #ddd;
    }
    th { background-color: #f2f2f2; }
  </style>
</head>

<body>
  <h3> Cost Optimization Framework </h3>
  <p>Please review the instance status report for the day.</p>
  
  <br>
  
  <table>
    <tr>
      <th>Cloud Provider</th>
      <th>Instance Name</th>
      <th>Instance IP</th>
      <th>Instance Status</th>
      <th>Instance Shutdown Time</th>
      <th>Instance Tag</th>
    </tr>
      __TABLE_ROWS_PLACEHOLDER__
  </table>

  <br>

  <p><i>*This email was auto-generated on __TIMESTAMP__. DO NOT respond to this email, for any query or concern contact costoptimization@rtlab.com.</i></p>
  <p>Author : Shaikh Sayeed</p><br>

  <p>Thanks</p>
  <p>Team CoreIT</p>

</body>
</html>
